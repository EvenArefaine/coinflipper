$(".btn").click(function () {
   var number = Math.floor(Math.random()*2)+1;
   $("img").attr("src","images/side"+number+".png");
   playSound(number);
   if (number===1){
      $("h1").text("Heads");
   }
   else{
      $("h1").text("Tails");
   }
})


// playSound function creates an audio object based on the auido file provided and calls on play method;
function playSound(number){
   var audio = new Audio("sounds/song"+number+".mp3");
   audio.play();
}
